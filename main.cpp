#include <iostream>
#include <cstring>
using namespace std;

#include "any_vector.h"

int main()
{
    char dummy_string[] = "This is but a dummy string.";
    uint32_t dummy_vec[] = { 30762, 219186, 0x285983, 1337 };

    cstd::any_vector my_vector;

    my_vector.push_back<char*>(dummy_string, strlen(dummy_string));
    my_vector.push_back<uint32_t*>(dummy_vec, 4);

    printf("%s\n", my_vector.get_memory<char*>(0));

    for(uint64_t i = 0; i < 4; ++i)
        printf("%X\n", my_vector.get_memory<uint32_t*>(1)[i]);
    return 0;
}
