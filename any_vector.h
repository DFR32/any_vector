#ifndef ANY_VECTOR_H
#define ANY_VECTOR_H
#include <vector>
#include <cstdlib>
#include <cstring>
#include <type_traits>

namespace cstd
{
    struct MEMORY_PAGE
    {
        bool usable;
        uint64_t size;
        void* address;
        uint64_t unique_identifier;

        MEMORY_PAGE()
            : usable(false),
              size(0),
              address(0)
        {
            unique_identifier = ~((rand() + 1) * ((rand() % 100) + 1));
        }
    };

    class any_vector
    {
    private:
        std::vector<MEMORY_PAGE> memory_vector;

        template <typename T>
        MEMORY_PAGE* alloc_memory(uint64_t sz)
        {
            MEMORY_PAGE new_page;

            new_page.size = sz;
            new_page.address = malloc(new_page.size);

            // classic check, may differ on other platforms
            if(new_page.address == 0)
            {
                new_page.usable = false;
                return (MEMORY_PAGE*)0;
            }
            new_page.usable = true;

            memory_vector.push_back(new_page);
            return &(memory_vector.back());
        }

        MEMORY_PAGE& at(uint64_t idx)
        {
            return memory_vector.at(idx);
        }

    public:

        any_vector()
        {
        }

        ~any_vector()
        {
        }

        template <typename T>
        T get_memory(uint64_t idx)
        {
            MEMORY_PAGE& dst = at(idx);

            if(!dst.usable)
                return T();

            if(dst.address == 0)
                return T();

            return reinterpret_cast<T>(dst.address);
        }

        template <typename T>
        void push_back(T data, uint64_t sz)
        {
            MEMORY_PAGE* page;
            uint64_t total_size = 0;

            if(std::is_pointer<T>::value)
            {
                typedef typename std::remove_pointer<T>::type deref_T;

                total_size = sz * sizeof(deref_T);

                page = alloc_memory<T>(total_size);
                memcpy(page->address, data, total_size);
            }
            else
            {
                total_size = sz * sizeof(T);

                page = alloc_memory<T>(total_size);
                memcpy(page->address, &data, total_size);
            }
        }

        void remove(uint64_t idx)
        {
            MEMORY_PAGE& pg = at(idx);

            if(!pg.usable)
                return;

            pg.usable = false;
            pg.size = 0;

            if(pg.address != 0)
            {
                free(pg.address);
                pg.address = 0;
            }

            memory_vector.erase(memory_vector.begin() + idx);
        }
    };
}
#endif // ANY_VECTOR_H
